# Jeu de rôle 

Le jeu de rôle est l'un des nombreux aspects de apprentissage expérientiel et peut être utilisé pour comprendre des concepts complexes. Lorsqu'il est axé sur les interactions humaines, il constitue un lieu sûr où les participant·es peuvent s'exprimer et jouer ce qui ne peut être dit ou ce qui a été un point aveugle. 

* Même des situations simples, comme aller au restaurant, préparer un repas, faire du camping ou aller à l'épicerie, peuvent vraiment éclairer les joueur·ses. Les points aveugles deviennent facilement visibles.
* En s'exerçant à jouer différents rôles, les participant·es commencent à apprendre ce qu'est la vie d'autres personnes qui jouent réellement ces rôles dans la société, au travail ou en famille. Bien que cela puisse se faire de manière beaucoup plus légère, cela peut aider à créer les bases d'une plus grande empathie envers les autres.
* Le jeu de rôle est également un excellent moyen pour les participant·es de faire appel à leur imagination. Naturellement, ils·elles ne savent probablement pas tout du rôle qu'ils·elles jouent, ce qui leur donne l'occasion de créer des situations, des problèmes à résoudre et d'expérimenter la façon dont ce rôle peut être assumé.
* Dans les jeux de rôle, les participant·es peuvent découvrir d'autres cultures ou voyager dans le temps.
* Le jeu de rôle peut aider à découvrir les détails des interactions entre les personnes.

## Chorégraphie

<img src="../assets/img/jeu-de-role.png" width="640" />

### Échauffer le groupe 

Cette étape consiste généralement à présenter aux participants un problème ou une situation, avant le jeu de rôle proprement dit. Nous préparons quelque chose et cela ouvre l'appétit. La réunion de jeu de rôle proprement dite peut également commencer par quelques courts exercices d'échauffement. 

### Choisir les rôles

Dans cette étape, les personnages et leurs caractéristiques sont esquissés et les participant·es se portent volontaires pour un rôle ou bien l'animateur·rice attribue les rôles. Comme pour l'échauffement, cette étape peut être réalisée à l'avance ou pendant la réunion de jeu de rôle proprement dite. En tant qu'animateur·rice, veillez à ne pas attribuer les rôles en fonction des suggestions des participant·es, car cela peut mettre un·e participant·e dans une situation inconfortable ou le·la stéréotyper.

### Mise en scène

Le début d'un scénario et le contexte sont établis et les rôles sont réaffirmés.

### Préparer les observateur·rices et les groupes de soutien 

Pour s'assurer que l'ensemble du groupe reste impliqué, nous utilisons des rôles supplémentaires, ceux d'observateur·rice et de soutien. 

Par exemple, les observateur·rices peuvent évaluer le réalisme du jeu de rôle, réagir à l'efficacité et aux séquences du comportement des acteur·rices, et décrire leur sentiment et leur façon de penser des rôles représentés. 

Certains rôles bénéficient d'un grand soutien dans la vie réelle, d'autres non. Cela peut également être inclus. Dans ce cas, le contexte culturel est évalué et esquissé.

### Mettre en scène 

Les joueur·ses endossent les rôles et "vivent" spontanément la situation du début à la fin de celle-ci.

### Discuter et évaluer

Dans cette étape, le scénario résultant et les actions du jeu de rôle sont examinés, l'objectif est discuté et la prochaine mise en scène est élaborée. Une option consiste à utiliser [l'art de la conversation ciblée](../retrospectives/Focused-conversation.md) pour cela.

### Remettre en scène

Au cours de cette étape, de nouvelles interprétations des rôles sont partagées et de nouvelles possibilités de causes et d'effets sont explorées.

### Discuter et évaluer

Dans cette étape, le scénario résultant et les actions du jeu de rôle sont examinés, l'objectif est discuté et la prochaine mise en scène est élaborée.

...

### Partager les expériences et généraliser

La situation problématique est liée aux problèmes actuels et aux expériences réelles d'une manière non menaçante.
