# Un gitbook des chorégraphies

[Un gitbook des chorégraphies](https://la_zone.gitlab.io/choregraphies/) pour partager les connaissances en matière de facilitation et apprendre les un·es des autres. Les chorégraphies peuvent varier d'un simple mouvement à une pirouette, de balancements en 8 à une glissade électrique : c'est une danse complète du début à la fin. 

* [Facilitation individuelle](un-a-un/README.md)
* [Rétrospectives](retrospectives/README.md)
* [Enoncés de problématiques](enonce-du-probleme/README.md)
* [Rétroplanning](retroplanning/README.md)
* [Jeu de rôle](jeu-de-role/README.md)
* [Documents](papers/README.md)
* [Livres](livres/README.md)

## Notions 

* Lorsque vous adoptez une chorégraphie, pensez à n'adopter que l'essentiel et à chorégraphier avec les participant·es ce qui concerne le contexte et le but.
* N'hésitez pas à améliorer les pages existantes. C'est aussi un exercice pratique pour ne pas toujours demander la permission.

## Participer via le dossier sur Gitlab

Les pages du gitbook des chorégraphies sont statiques, construites avec GitLab CI. Vous pouvez participer via le dossier sur Gitlab. Ou si vous ne voulez pas vous embêter avec tout cela, envoyez ce que vous souhaitez partager à l'une des personnes qui participent.

* Installer Codium (ou VSCode ou tout autre IDE ou éditeur simple que vous préférez)
* Créer un [compte Gitlab](https://gitlab.com)
* Obtenir les permissions d'accès au [groupe la_zone](https://gitlab.com/la_zone)
* Cloner le [repository](https://gitlab.com/la_zone/choregraphies)
* Nous apprécions que vous utilisiez les branches pour ajouter des pages.
* Utiliser la [markdown](https://gitbookio.gitbooks.io/documentation/content/format/markdown.html)

## Construire le site localement

La construction d'un site local et le fonctionnement d'un serveur de développement local ne sont pas nécessaires. Beaucoup de [problèmes](https://stackoverflow.com/questions/64211386/gitbook-cli-install-error-typeerror-cb-apply-is-not-a-function-inside-graceful) avec cela de toute façon. 

Mais si vous voulez vraiment avoir un serveur local (comme moi), installez 'gitbook-cli' :

```
:~/choregraphies$ npm install gitbook-cli -g

```

Then 'graceful-fs' 4.2.0 dans les modules de nœuds 'npm' de 'gitbook-cli' :

```
:~/choregraphies$ cd /home/<user>/.nvm/versions/node/v16.4.0/lib/node_modules/gitbook-cli/node_modules/npm/node_modules/
:~/.nvm/versions/node/v16.4.0/lib/node_modules/gitbook-cli/node_modules/npm/node_modules$ npm install graceful-fs@4.2.0 --save
```
Retournez dans le dossier supérieur du projet et réinstallez gitbook-cli et gitbook :

```
:~/choregraphies$ npm install gitbook-cli -g
:~/choregraphies$ gitbook -V
CLI version: 2.3.2
Installing GitBook 3.2.3
/home/nina/.nvm/versions/node/v16.4.0/lib/node_modules/gitbook-cli/node_modules/npm/node_modules/graceful-fs/polyfills.js:287
      if (cb) cb.apply(this, arguments)
                 ^

TypeError: cb.apply is not a function
    at /home/nina/.nvm/versions/node/v16.4.0/lib/node_modules/gitbook-cli/node_modules/npm/node_modules/graceful-fs/polyfills.js:287:18
    at FSReqCallback.oncomplete (node:fs:196:5)
```
Ensuite, Control-cliquez sur le lien `polyfills.js` dans cette réponse (tel quel) et décommentez ces trois déclarations avec '//', comme ceci :

``` javascript
  // fs.stat = statFix(fs.stat)
  // fs.fstat = statFix(fs.fstat)
  // fs.lstat = statFix(fs.lstat)
```

Exécutez à nouveau 'gitbook -V' et construisez.

```
:~/choregraphies$ gitbook -V 
:~/choregraphies$ gitbook build
...
info: >> generation finished with success in 0.6s !
```

Le gitbook peut maintenant être servi localement :

```
:~/choregraphies$ gitbook serve
Live reload server started on port: 35729
Press CTRL+C to quit ...
...
info: >> generation finished with success in 0.6s ! 

Starting server ...
Serving book on http://localhost:4000
```
