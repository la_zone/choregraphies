# L'art de la conversation ciblée

Une "conversation ciblée" est un processus relativement simple en quatre niveaux, mené par un facilitateur qui pose une série de questions pour susciter des réponses qui amènent le groupe à passer de la surface d'un sujet à ses implications plus profondes. 

Vous pouvez également consulter un autre [bref résumé de l'art de la conversation ciblée (en anglais)](https://srinathramakrishnan.files.wordpress.com/2017/12/the-art-of-focussed-conversation.pdf).

<img src="../assets/img/focused-conversation.png" width="640" />

Les niveaux ci-dessous peuvent être utilisés comme un tout, ou individuellement dans les moments d'une autre chorégraphie où il semble y avoir des trous (composants manquants).

- [L'art de la conversation ciblée](#lart-de-la-conversation-ciblée)
  - [Questions de base](#questions-de-base)
  - [Questions de réflexion](#questions-de-réflexion)
  - [Questions d'interprétation](#questions-dinterprétation)
  - [Questions de résolution](#questions-de-résolution)

## Questions de base

Questions sur les faits et la réalité extérieure. Ces questions sont liées aux sens et aident un groupe à trouver un terrain d'entente sur un ensemble de données. Posez des questions ouvertes tout en étant suffisamment précises pour permettre une orientation claire. N'évitez pas de poser ces questions de base parce qu'elles sont << trop banales >>. Elles ne le sont pas. Elles constituent le plancher d'entente sur lequel un groupe peut s'appuyer.

Exemples de questions : Quels sont les objets que vous avez vus ? Quels mots ou phrases vous ont marqué ? Que s'est-il passé ? Qu'avez-vous entendu ?

## Questions de réflexion

Questions visant à susciter une première réaction personnelle aux données, une réponse interne, parfois des émotions ou des sentiments, des images cachées et des associations avec l'ensemble des données. Ces questions sont liées aux sentiments, aux humeurs, aux tonalités émotionnelles, aux souvenirs et aux associations et peuvent aider un groupe à se connecter aux mondes de l'intuition, de la mémoire, de l'émotion et de l'imagination. Les sondages peuvent être utilisés, mais il ne faut pas utiliser les sondages de type "aime ou n'aime pas". Les discussions en direct sont préférables.

Exemples de questions : A quoi cela vous fait-il penser ? Comment vous sentez-vous ? Où êtes-vous/étais vous surpris ? Où êtes-vous ravi ? Où avez-vous eu des difficultés ?

## Questions d'interprétation

Questions visant à trouver un sens, des valeurs, une signification et des implications. Ces questions sont liées à des couches de sens, d'objectif, de signification, d'implications, d'histoires et de valeurs, et facilitent une réflexion de niveau supérieur pour aider le groupe à attribuer une signification à l'ensemble des données et à générer des alternatives et des options si nécessaire. En tant que facilitateur, ne permettez pas l'insertion d'une signification précuite, l'intellectualisation, l'abstraction et les réponses justes ou fausses basées sur un jugement.

Exemples de questions : Que se passe-t-il maintenant ? De quoi s'agit-il ? Qu'est-ce que cela signifie pour nous ? Comment cela va-t-il nous affecter ? Qu'apprenons-nous de cette situation ? Quel sont les point de vues ?

## Questions de résolution

Questions visant à obtenir une résolution, à trouver une conclusion, à avoir plus d'implications, de nouvelles directions, et à clore la conversation. Ces questions sont liées au consensus, à la mise en œuvre, à l'action, permettant au groupe de rendre la conversation pertinente pour l'avenir. Si, à ce stade, le groupe est bloqué, n'insistez pas, revenez sur les étapes précédentes car le groupe n'est pas prêt à prendre une décision.

Exemples de questions : Quelle est notre réponse ? Quelle décision faut-il prendre ? Quelles sont les prochaines étapes possibles ? 