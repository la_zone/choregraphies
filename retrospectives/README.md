# Retrospectives

Dans notre contexte, une rétrospective est un regard sur des événements qui ont eu lieu ou des œuvres qui ont été produites dans le passé. 

<img src="../assets/img/mistakes-wheel.png" width="640" />

Les résultats peuvent être multiples : amélioration du projet et du produit, déblocage de processus précédemment bloqués, découverte d'implications cachées, décision d'explorer de nouvelles directions, résolution de conflits, produits bifurqués, ...

## Chorégraphie

Il existe de nombreuses chorégraphies pour les rétrospectives, et les meilleures rétrospectives sont dansées sur le moment et répondent à ce qui se présente, en combinant plusieurs mouvements de danse pour aider le groupe à trouver un état final approprié de la rétrospective.

Nous décrivons donc ici quelques-uns des mouvements de danse connus avec lesquels vous pouvez être créatif sur le moment.

* Satir temperature reading
* [L'art de la conversation ciblée](Focused-conversation.md)