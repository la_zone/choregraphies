# Exemple objectif : Une conception plus équitable de La Zone

L'ancien système de La Zone n'est plus adapté à cette tâche. Si nous acceptons cette affirmation, nous devons également conclure qu'aucun réglage fin de l'ancien système ne produira d'améliorations significatives. Quel type de système est nécessaire pour produire la percée que nous recherchons ?

- [Exemple objectif: Une conception plus équitable de La Zone](#exemple-objectif-une-conception-plus-équitable-de-la-zone)
  - [Type de transition](#type-de-transition)
  - [Quelques exemples des éléments](#quelques-exemples-des-éléments)
  - [Est-ce réaliste ?](#est-ce-réaliste-)
    - [Quel est le but de l'existence de La Zone ?](#quel-est-le-but-de-lexistence-de-la-zone-)
    - [L'algorithme d'ordonnancement pour plus d'équité](#lalgorithme-dordonnancement-pour-plus-déquité)
  - [Élément clé : Marché plusieurs à un](#élément-clé--marché-plusieurs-à-un)

## Type de transition

Jusqu'à présent, l'analyse des conversations, des documents et de la pratique dans La Zone et sur le marché suggère que nous passons de systèmes déterministes à des systèmes de recherche d'objectifs. En termes sociaux, nous passons de styles organisationnels "dictatoriaux" à "participatifs".

Pour effectuer ce type de transition, il suffit de changer de perspective et de passer d'une orientation "un à plusieurs" (gouvernement à co-traitant·es) à une orientation "plusieurs à un". 

Cela semble simple, mais les implications de cette idée sont énormes. Il n'y a pas de place dans la nouvelle conception du système pour l'ancien modèle variable/routine et les nouvelles conceptions du système comporteront un nombre croissant d'éléments. 

## Quelques exemples des éléments

Des éléments plus conscients du contexte à attendre d'une restrospective de La Zone, et de nouvelles informations sur la formation des coopératives d'équité en France :

* Déclarations générales d'objectifs convenus par les participant·es de La Zone
* Normes liées aux résultats
* Modèle économique de équité
* Plus de client·es que le seul marché gouvernemental, de préférence d'autres coopératives d'équité
* Des équipes intactes travaillant sur une longue période (plus d'un an) pour atteindre un objectif
* Affectation aux équipes sur la base des performances individuelles, plutôt que sur des distinctions fondées sur les croyances
* Triangulation du feedback : utilisation de plusieurs formes d'évaluation par plusieurs évaluateur·rices pour augmenter la validité et la fiabilité du feedback. Par exemple, des évaluations basées sur la performance comparée à des repères, à d'autres développeur·ses et auto-évaluation
* Structures d'apprentissage variées : Par exemple, autodidacte, en tête-à-tête, en petits groupes, conférence, étude sur le terrain, apprentissage, mentorat
* Informations et ressources numérisées
* ...

Aucun de ces éléments n'est totalement nouveau, mais comme l'ont montré les nombreuses tentatives infructueuses menées dans divers contextes, comme il apparaît de plus en plus clairement, l'équité dans les systèmes exige une approche plus intégrée et plus globale, et un système totalement nouveau.

## Est-ce réaliste ?

La mise en œuvre d'une perspective systémique peut toutefois être mise à mal par les structures organisationnelles, la gouvernance partagée, l'autonomie des autorités et les problèmes budgétaires permanents.

### Quel est le but de l'existence de La Zone ?

* Plusieurs structures de type coopératives tentent de travailler ensemble, en veillant à la fois à avoir une culture commune, et à rester ouvertes entre elles et vers l’extérieur.
* Travailler délicatement avec des personnes disparates sur des projets sociaux et environnementaux (difficilement accessibles aux petites structures) en restant attentif·ves à l'enthousiasme et au consentement.
* La Zone fait initialement référence à [La Zone (Paris](https://fr.wikipedia.org/wiki/La_Zone_(Paris)) : c’est la zone ambigüe entre la ville structurée (l’Etat) et le monde lointain (les indépendant·es). Le groupement se place dans cette interface pour permettre de faire cohabiter ces deux mondes. Je ne pense pas qu’il y ait une vision unique, et c’est justement ce qui en fait sa force, pour répondre différemment à des besoins différents.

### L'algorithme d'ordonnancement pour plus d'équité

Il n'y a pas eu de mécanismes d'équité dans la licorne dans le passé. Du gouvernement aux interfaces (Octo, La Zone, et Arolla), il existait un simple système de rotation. 

* Le système de rotation est défini (de manière ambigüe et fait beaucoup parler) par le marché. Le fait de passer à un autre attributaire se faisait plutôt en fonction de la capacité (administrative, trésorie) et de l'envie (compatibilité de valeurs) que pour des besoins techniques.
* Les mesures de capacité et d'envie sont surtout une question de ressenti. Pour l’envie, il faut que ça corresponde à une charte. L’aspect financier et administratif n'est pas encore clair.
* Une bonne partie de travailleuses et travailleurs prestataires chez beta.gouv.fr ne font pas partie d’un des attributaires et sont recruté·es directement par la startup. Il y a pu y avoir des efforts pour communiquer sur les postes disponibles, mais il est probable que la facilité de recruter quelqu'un·e qu'on connait prenne vite le dessus.
* Des attributaires sont blâmés : "Des milliards dépensés pour se substituer à l’Etat enquête sur la République des consultants", l'OBS. 

## Élément clé : Marché plusieurs à un

"Changer la perspective d'une orientation un à plusieurs vers une orientation plusieurs à un", c'est très abstrait. Je l'ai gardé abstrait parce que dans ce contexte, ça peut être appliqué à de nombreuses directions, informations, économies (énergie, entropie),... Par exemple, on pourrait passer de la vision de La Zone comme un système dans lequel un·e client·e offre des contrats possibles à de nombreux·euses co-traitant·es, à un système dans lequel il y a de nombreuses ressources d'échanges économiques accessibles par un·e co-traitant·e, dont un seul sera le gouvernement. Il en va de même pour les sous-traitant·es et les autres indépendant·es individuel·les à l'égard des co-traitant·es.

Par exemple, lorsque j'aurai du travail de programmation rémunéré et que j'aurai du travail administratif à faire, je demanderai à quelqu'un de La Zone qui a cette expertise, et je paierai pour cela. De même, si j'ai besoin d'un·e designer UX ou d'un·e facilitateur·rice, ou ... Pour cela, j'ai besoin de savoir qui est qui. Donc, par exemple, un élément de conception dans un système d'équité serait une solution à cela. Notre propre site de marché du travail, où je peux voir qui a quelle spécialité et qui est libre, pourrait être une solution. Il serait encore mieux d'avoir plusieurs solutions.

Note : Si moi, en tant que personne seule, j'utilise le "népotisme" (j'ai une préférence pour quelqu'un·e, et c'est le cas, c'était mon rêve de me développer avec d'autres femmes), cela n'affecte pas le système entier autant que lorsque le gouvernement ou un·e co-traitant·e le fait.

Si nous souhaitons un système avec équité, nous devrons trouver ou fonder des marchés du travail (ouverts ou semi-ouverts) avec de vrai·es client·es, de préférence d'autres coopératives d'équité. Je proposerai un projet de construction d'un site de ["marché plusieurs à un"](Exemple-projet-marche.md), j'essaierai de le faire financer et de mettre sur pied une équipe mixte pour le réaliser.


