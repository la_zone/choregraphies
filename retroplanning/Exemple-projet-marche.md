# Exemple Marché plusieurs à un

Pour facilitation [d'une conception plus équitable de la zone](Exemple-fairness.md), je proposerai un projet de construction d'un site de "marché un à plusieurs". L'intention est également d'incorporer d'autres éléments de conception clés pour plus de équité.

- [Exemple Marché plusieurs à un](#exemple-marché-plusieurs-à-un)
  - [Plan site version 0.1](#plan-site-version-01)
  - [Technologie](#technologie)
  - [Équipe](#équipe)
  - [Clés plus](#clés-plus)
  - [Financement](#financement)

## Plan site version 0.1

* Authentification de l'utilisateur (inscription, connexion, déconnexion). Parce qu'un utilisateur peut être à la fois un client offrant un emploi et un indépendant à la recherche d'un emploi, il n'est pas nécessaire de séparer les clients et les indépendants.
* Page tableau de bord 
  * Liste des offres d'emploi proposées par l'utilisateur (il peut n'y en avoir aucune) et bouton pour créer/modifier/supprimer une offre.
  * CV de l'utilisateur (peut être nul) et bouton pour créer/modifier/supprimer le CV.
  * Liste des offres d'emploi demandées (peut être inexistante, avec pour résultat "non libre") et bouton pour créer/modifier/supprimer une demande.
* Page avec formulaire d'offre d'emploi (creation, edit, delete)
* Page avec formulaire de demande d'emploi (creation, edit, delete)
* Page avec formulaire CV (creation, edit, delete)
* Pages d'offres et demandes
* Pages details d'un offre
* Pages details d'un demande
* Design UX
* Test
* Premier déploiement 0.1
* ...

## Technologie

* Python
* Django
* Github ou Gitlab ?
* First déploiement où ? Heroku ?
* ...

## Équipe

* Product owner : de La Zone. 
* Product manager de La Zone : responsable du suivi de l'avancement global du développement.
* Facilitator
* Coach
* Une équipe avec un ensemble équilibré de compétences et, de ce fait, aussi mixte que possible, et au moins quatre programmeurs juniors.

## Clés plus

L'intention est également d'incorporer d'autres éléments de conception clés pour plus d'équité, tels que 
* Triangulation du feedback : utilisation de plusieurs formes d'évaluation par plusieurs évaluateurs pour augmenter la validité et la fiabilité du feedback. Par exemple, evaluations basées sur la performance comparée à des repères, à d'autres développeurs et auto-évaluation
* Structures d'apprentissage variées : autodidacte, en tête-à-tête, en petits groupes, conférence, étude sur le terrain, apprentissage, mentorat. Soutien de diverses personnes de la Zone pour cela.
* Concevoir et mettre en œuvre des mesures de progression du développement pour une culture de pilotage.
* Élaboration d'une norme de pratiques de sécurité pour le développement et le déploiement.
* ...

## Financement

* Le financement ne doit pas nécessairement provenir de beta.gouv, même s'il sera également avantageux pour eux de trouver plus rapidement des indépendants adaptés.
* Comme notre équipe sera mixte, une petite partie du financement peut provenir de La Zone elle-même. Cela montre aussi l'engagement de La Zone pour plus de équité.
* Peut-être pourrons-nous trouver un financement de l'UE car ce projet sert aussi à inclure les minorités marginalisées.
* ...
