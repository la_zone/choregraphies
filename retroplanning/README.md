# Rétroplanning

Rétroplanning avec alternatives, boucles de feedback et évitement des obstacles. L'intention ici est de trouver des objectifs concrets possibles sur lesquels travailler. Il n'est pas nécessaire de travailler sur tous les objectifs. Agit agile!

Résultats : Un planning facile à suivre.

## Chorégraphie

### L'ordre

Identifiez les étapes et la séquence nécessaires pour atteindre votre objectif choisee.

* Rédigez une liste aléatoire de toutes les étapes et ressources dont vous avez besoin pour atteindre votre objectif.
* Estimez le temps et les ressources (argent et personnes) dont vous avez besoin pour réaliser chaque étape de la liste.
* Regroupez les étapes aléatoires en groupes et mettez-les en ordre. Pour cette activité, vous pouvez utiliser des notes autocollantes sur une table ou un mur, en écrivant une étape sur chaque note. Vous pourrez alors facilement les déplacer dans les groupes. Enfin, pensez à créer un plan de départ pour commencer le séquençage.
* Créez un calendrier en couchant votre plan sur papier ou [Miro map](https://miro.com/). Il est beaucoup plus facile de créer votre calendrier en travaillant à rebours - en gardant la fin en tête.

### Le temps

Attribuant des dates d'échéance spécifiques à l'objectif principal (et aux étapes connexes) si cela a du sens.

* Déterminez la date à laquelle votre objectif doit être atteint.
* Identifiez la dernière étape que vous devez réaliser avant la date d'échéance de l'objectif.
* Identifiez l'avant-dernière étape ; l'avant-dernier avant le dernier, et continuez jusqu'à ce que vous ayez fini de mettre toutes les étapes dans l'ordre inverse. Il se peut que vous deviez déplacer un peu les choses jusqu'à ce que vous finalisiez le plan.

### Feedback

Cherchez du soutien.

* Demandez l'avis de quelqu'un pour vous assurer que vous n'avez pas négligé un élément essentiel.
* Vérifiez si les ressources identifiées sont disponibles.

### Suivi

* Agir.
* Suivi des progrès.
