# Summary

* [Introduction](README.md)

### Facilitation individuelle

* [Chorégraphie](un-a-un/README.md)

### Rétrospectives

* [Chorégraphie](retrospectives/README.md)
* [L'art de la conversation ciblée](retrospectives/Focused-conversation.md)

### Enoncés de problématiques

* [Chorégraphie](enonce-du-probleme/README.md)/
* [Exemple d'etat actuellement perçu](enonce-du-probleme/Exemple-d'etat-actuellement.md)
* [Exemple d'état souhaité](enonce-du-probleme/Exemple-d'état-souhaité.md)
* [Enquête sur la diversité, l'équité et l'inclusion de la Zone](enonce-du-probleme/Enquête-dei.md)

### Rétroplanning

* [Chorégraphie](retroplanning/README.md)
* [Exemple objectif: Une conception plus équitable de la Zone](retroplanning/Exemple-fairness.md)
* [Exemple Marché un à plusieurs](retroplanning/Exemple-projet-marche.md)

### Jeu de rôle

* [Chorégraphie](jeu-de-role/README.md)
  
### Ressources

* [Documents](papers/README.md)
* [Livres](livres/README.md)

