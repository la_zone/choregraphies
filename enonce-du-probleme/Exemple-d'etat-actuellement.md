# Exemple d'état actuellement perçu

De [la manière dont on fait la promotion de l'égalité et de la mixité professionnelles ainsi que de la prévention et de la lutte contre toutes les formes de discriminations](https://www.notion.so/la-mani-re-dont-on-fait-la-promotion-de-l-galit-et-de-la-mixit-professionnelles-et-de-pr-vention-c419e9cf90984f15946d03add9d2e71c) :

> Reconnaître que notre réseau manque de diversité (cf manifeste), que la majorité d'entre nous n’est pas à l’aise pour le reconnaître, qu'on veut se donner les moyens de changer ça et qu'on n’a aucune certitude sur la manière d’y parvenir.

Nous risquons de nous contenter d'un simple discours sur l'augmentation de la diversité et la diminution de la discrimination :

* Nous ne savons pas ce que nous ne savons pas - nos objectifs sont trop vagues.
* Nous sommes pris dans les détails quotidiens du développement et nous n'investissons pas le temps de réflexion nécessaire à l'élaboration d'un plan.
* Sans soutien, nous manquons de confiance dans nos aspirations.

En bref, un désordre typique de la résolution de problèmes de la diversité, l'équité et l'inclusion [qui doit être nettoyé](../README.md).

## Recherches supplémentaires

* [Ressources pour comprendre plus le contexte général](ressources)
* [Enquête sur la diversité, l'équité et l'inclusion de la Zone](Enquête-dei.md)
