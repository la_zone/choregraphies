# Enquête sur la diversité, l'équité et l'inclusion de la Zone

Enquête anonyme. Après la collecte des réponses, les réponses individuelles seront immédiatement effacées.

## Démographie

* À quels groupes raciaux/ethniques appartenez-vous ?
* Comment décrivez-vous votre identité ?
* Vous identifiez-vous comme une personne en situation de handicap ou comme une personne ayant des besoins en matière d'accessibilité ?

## Diversité

Sur une échelle de 1 à 5, où 1 signifie "Pas du tout d'accord" et 5 "Tout à fait d'accord", veuillez évaluer les affirmations suivantes ? (ajoutez des zones de saisie de texte pour commentaires additionels)

* Déclaration : "La Zone" valorise la diversité.
* Énoncé : Toutes et tous les co-traitant·es comprennent que la diversité est essentielle à notre réussite future.
* Affirmation : "La Zone" investit du temps et de l'énergie dans la constitution d'équipes diversifiées.

## Inclusion

Sur une échelle de 1 à 5, où 1 correspond à "Pas du tout d'accord" et 5 à "Tout à fait d'accord", comment évaluez-vous les affirmations suivantes ? (ajoutez des zones de saisie de texte pour commentaires additionels)

* Déclaration : J'ai l'impression que mes antécédents et mon identité uniques (essentiellement mes différences) sont valorisés dans "La Zone"/la traitance/l'équipe de projet.
* Affirmation : Je ressens un sentiment d'appartenance dans "La Zone"/la traitance/l'équipe de projet.
* Déclaration : Je me sens respecté·e par mes collègues.

## Équité

Sur une échelle de 1 à 5, où 1 correspond à "Pas du tout d'accord" et 5 à "Tout à fait d'accord", comment évaluez-vous les affirmations suivantes ? (ajoutez des zones de saisie de texte pour commentaires additionells)

* Déclaration : Les processus de promotion des rôles sont transparents.
* Affirmation : Les personnes de toutes origines et de toutes identités ont des chances équitables de faire progresser leur carrière dans "La Zone"/la traitance/l'équipe de projet.
* Déclaration : Je me sens soutenu·e dans ma progression de carrière dans "La Zone"/la traitance/l'équipe de projet.
