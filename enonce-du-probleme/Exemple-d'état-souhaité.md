# Exemple d'état souhaité

Comment pouvons-nous aider /ut7 à découvrir des objectifs concrets sur lesquels travailler ?

## Pas seulement des paroles en l'air

* Ne demandez pas aux minorités marginalisées de [nettoyer vos dégâts](../README.md). Montez !
* Et si nous avons besoin d'expertise, payez bien ces personnes. Joignez l'acte à la parole !

## Notions croyances corrigées

Si la discrimination est motivée par une partialité fondée sur des croyances et une mauvaise spécification ([The Dynamics of Discrimination:Theory and Evidence](../papers/README.md)), le critère de bien-être est clair : les croyances incorrectes sont inefficaces.

Les campagnes qui visent à corriger les croyances amélioreront les résultats tant sur le plan de l'efficacité que de l'équité. Mais, pour encourager ces changements, il faut agir tant au niveau individuel qu'au niveau sociétal.

Comme les processus psychologiques et sociaux qui incitent au changement de comportements dépendent de nombreux facteurs, [différentes théories bien établies](../papers/README.md) mettent l'accent sur différents aspects susceptibles de déclencher le changement.

* Montrer que la discrimination affecte toute la société et qu’il faut y mettre un terme.
* Rendre attractifs et profitables les changements visant à l’élimination de la discrimination.
* Fournir des modèles de rôles afin d’inciter les gens à agir pour mettre fin à la discrimination.
* Une approche relativement complexe dans le domaine des discriminations consiste à déclencher et à appuyer des processus de développement personnel au cours d’une période prolongée.
* ...

## Evaluations plus objectives

Rendre les évaluations plus objectives améliorera également l'efficacité et l'équité.
