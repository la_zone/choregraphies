# Enoncés de problématique

Le problème est défini comme la différence entre l'état actuellement perçu et un état souhaité.

<img src="../assets/img/problems.png" width="640" />

Résultats : une liste d'éléments actuellement perçus et une liste d'éléments souhaités.

## Chorégraphie

### Etat actuellement perçu

L'accent est mis ici sur "perçu", pour éviter la paralysie du paradigme, ou mumpsimus, "persistance dans une croyance erronée", c'est-à-dire la tentative d'interpréter l'expérience actuelle à l'aide d'anciens modèles et métaphores qui ne sont plus appropriés ou utiles. Pour savoir lesquelles, il faut qu'elles soient explicites.

* Décrivez les éléments de l'état actuel de manière aussi précise que possible. Ne vous préoccupez pas du bien ou du mal, ceux-ci n'existent pas.
* S'il n'est pas possible d'être précis pour un élément observé, faites des recherches supplémentaires. Demandez-vous si d'autres perspectives sur l'élément sont possibles.

### Etat souhaité

* Brainstorm que voulez-vous accomplir ?
* Ensuite, réécrivez chacun des objectifs de l'état souhaité de la manière la plus précise possible. Demandez-vous pourquoi il est important et s'il est réaliste.

## Recommandations

* Faites participer entre cinq et neuf personnes à tout brainstorming.  Avec moins de cinq, la divergence (brain storming) devient difficile, avec plus de neuf, la convergence (prise de décision) devient difficile.
