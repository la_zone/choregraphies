# Facilitation individuelle

Dans le cadre d'une facilitation individuelle, les deux personnes peuvent acquérir une grande compréhension en travaillant individuellement sur les problèmes, car les progrès dans leurs directions spécifiques peuvent être développés à leur propre rythme.

focused-conversation

<img src="../assets/img/wheel-balanced-choreography.png" width="640" />

Les chorégraphies sont très différentes, qu'il s'agisse de travailler sur les voix des prétendants, les résistances ressenties, le stress et le déni, la concrétisation des passions, etc.