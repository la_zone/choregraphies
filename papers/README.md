# Documents

## Discrimination

* [The Dynamics of Discrimination: Theory and Evidence](https://www.aeaweb.org/articles?id=10.1257/aer.20171829), J. Aislinn Bohren, Alex Imas, Michael Rosenberg, American Economic Review, vol. 109, no. 10, October 2019, (pp. 3395-3436)
* [Le coût économique des discriminations](https://www.strategie.gouv.fr/publications/cout-economique-discriminations), Rapport à la ministre du Travail, de l’Emploi, de la Formation professionnelle et du Dialogue social, et au ministre de la Ville, de la Jeunesse et des Sports.
* [Gender Index France](https://www.genderindex.org/country/france/), OECD dev
* ...

## Changement de comportement

* [Understanding Attitudes and Predicting Social Behaviour](https://www.semanticscholar.org/paper/Understanding-Attitudes-and-Predicting-Social-Ajzen-Fishbein/2303e303f57636618d9c8d5b0050c558a565e835), Ajzen and Fishbein, 1980. 
* [Exercise of personal agency through the self-efficacy mechanism](https://www.semanticscholar.org/paper/Exercise-of-personal-agency-through-the-mechanism.-Bandura/ee2ee40a013d23675b7f4a101c7759a94a709f3f), Bandura, A., 1992. 
* [Le Health Belief Model (HBM), ou Modèle de croyance en santé](https://fr.wikipedia.org/wiki/Health_belief_model)
* [Satir Change Model (pdf)](https://satirglobal.org/wp-content/uploads/2020/04/Satir-Change-Model.pdf), A Model For Individuals, Groups & Organizations During A Change Process, Satir Global
* [Innovation, Change Theory and the Acceptance of New Technologies (pdf)](http://www.educationaltechnology.ca/publication_files/unpublishedpapers/change_theory.pdf) : A Literature Review, Alec Couros, 2003
* [Analyse entrée-sortie](https://fr.wikipedia.org/wiki/Analyse_entr%C3%A9e-sortie)
* ...